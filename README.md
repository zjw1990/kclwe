Requirements
--------
- Python 3
- NumPy
- SciPy


Usage
--------

```
python3 kclwe.py --src_path SRC_PATH --trg_path TRG_PATH --dic_train_path TRAIN_DIC dic_test_path TEST_DIC --gamma GAMMA --weight WEIGHT --kappa KAPPA --yita YITA
```

'SRC_PATH' and 'TRG_PATH' refers to the location of the monolingual word embeddings trained by word2vec. 

'TRAIN_DIC' refers to the path of training dictionary. 

'TEST_DIC' refers to the path of test dictionary.

'GAMMA' refers to the hyperparameter of rbf kernel.

'WEIGHT' refers to the re-weighting parameter. 

'KAPPA' and 'YITA' refer to the parameters in KCCA, which we suggest to fix it by 10 and 0.7


Dataset
--------
We proive EN-ZH trained embeddings in ./embeddings and the dictionary ./dictionary. The dictionary is a modification to [Conneau et al.(2017)](https://github.com/facebookresearch/MUSE). Other dictionaries and trained embeddings can be found at [vecmap](https://github.com/artetxem/vecmap), which is an extension of [Dinu et al. (2014)](http://clic.cimec.unitn.it/~georgiana.dinu/down/).


Reproduction
--------
We also provide parameters to reproduce our result. Due to the different relationships between languages, each language language pair have different parameters to discribe the relationships.

English - German word translation: gamma = 0.141, weight = 0.326 

English - Finnish word translation: gamma = 0.37, weight = 0.38
                                                              
English - Italian word translation: gamma = 0.05, weight = 0.3
                                                                                                                                                                                                                                                                   
English - Chinese word translation gamma = 0.527,  weight = 0.552





English - Spanish word translation gamma = 0.214, weight = 0.495

Citation
--------
If you use this software or our Chinese language resource for academic research, please cite our paper as follows:

```
@inproceedings{jiawei2020,
  author    = {Jiawei, Zhao  and  Andrew, Gilman},
  title     = {Non-Linearity in mapping based Cross-Lingual Word Embeddings},
  booktitle = {Proceedings of the 12th International Conference on Language Resources and Evaluation (LREC 2020)},
  year      = {2020},
  pages     = {}
}
```
