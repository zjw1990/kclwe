# Copyright (c) 2020 Jiawei Zhao <j.zhao2@massey.ac.nz>

# Copyright (c) 2020 Andrew Gilman <a.gilman@massey.ac.nz>

#

# Permission is hereby granted, free of charge, to any person obtaining

# a copy of this software and associated documentation files (the

# "Software"), to deal in the Software without restriction, including

# without limitation the rights to use, copy, modify, merge, publish,

# distribute, sublicense, and/or sell copies of the Software, and to

# permit persons to whom the Software is furnished to do so, subject to

# the following conditions:

#

# The above copyright notice and this permission notice shall be

# included in all copies or substantial portions of the Software.

#

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,

# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF

# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND

# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE

# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION

# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION

# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys
import argparse
import time
import numpy as np 
import scipy.linalg as LA
from numpy import matlib
from sklearn.metrics import pairwise_kernels
from sklearn.metrics.pairwise import rbf_kernel, laplacian_kernel, linear_kernel, sigmoid_kernel, polynomial_kernel
from sklearn.preprocessing import KernelCenterer
import datetime
def load_data(path):
    
    vol_src,emb_src = read_data(path[0])
    vol_trg, emb_trg = read_data(path[1])
    
    #build test dictionary
    src_word2ind = {word: i for i, word in enumerate(vol_src)}
    trg_word2ind = {word: i for i, word in enumerate(vol_trg)}
    
    src_indices_train,trg_indices_train = load_dictionary(path[2], src_word2ind, trg_word2ind)
    src_indices_test,trg_indices_test = load_dictionary(path[3], src_word2ind, trg_word2ind)

    emb_src = np.array(emb_src, dtype="float32")
    emb_trg = np.array(emb_trg, dtype="float32")

    return vol_src, emb_src, vol_trg, emb_trg, src_indices_train, trg_indices_train, src_indices_test, trg_indices_test
        
def read_data(path):
    f = open(path, encoding="utf-8", errors='surrogateescape')
    dtype = np.float32
    header = f.readline().split(' ')
    count = np.int(header[0])
    dim = int(header[1])
    words = []
    matrix = np.empty((count, dim), dtype=dtype)
    for i in range(count):
        word, vec = f.readline().split(' ', 1)
        words.append(word)
        matrix[i] = np.fromstring(vec, sep=' ', dtype=dtype)
    return words,matrix


# find corresponding indices of a dictionary from two vol
'''
    input: path: the path for the corresponding dictionary
           src_word2ind: dic, the mapping from source word to word indices
           trg_word2ind: dic, the mapping from target word to word indices
    output: 
           src_indices: list, the indices of the words in the dictionary
           trg_indices: list, the indices of the words in the dictionary
'''

def load_dictionary(path,src_word2ind,trg_word2ind):
    src_indices = []
    trg_indices = []

    f = open(path, encoding="utf-8", errors='surrogateescape')
    for line in f:
        src_word,trg_word = line.split()
        try:
            src_ind = src_word2ind[src_word]
            trg_ind = trg_word2ind[trg_word]
            src_indices.append(src_ind)
            trg_indices.append(trg_ind)
        except KeyError:
            print("OOV words occurs")
            
    return src_indices, trg_indices



# mean_center
def mean_center(x):
    x = x - np.mean(x , axis = 0)
    return x

def length_normalize(x):
    norms = np.sqrt(np.sum(x**2, axis=1))
    norms[norms == 0] = 1
    x /= norms[:, np.newaxis]
    return x


def hardoonKCCA(Kx,Ky, kapa, yita):
    Kx = K_mean_center(Kx)
    Ky = K_mean_center(Ky)
    # decompose Kx, Ky
    Rx, RxSize, RxIndex = PGSO(Kx, yita)
    Ry, RySize, RyIndex = PGSO(Ky, yita)
    Rx = delZeros(Rx)
    Ry = delZeros(Ry)
    # Creating the new Z and R
    Zxx = Rx.T.dot(Rx)
    Zxy = Rx.T.dot(Ry)
    Zyy = Ry.T.dot(Ry)
    Zyx = Zxy.T
    tEyeY = np.eye(np.shape(Zyy)[0])
    tEyeX = np.eye(np.shape(Zxx)[0])
    import scipy.linalg as LA
    # Compute n_alpha eigenproblem
    B = Zxx + kapa*tEyeX
    S = LA.cholesky(B).T
    invS = LA.inv(S)
    A = invS.dot(Zxy).dot(LA.inv(Zyy+kapa*tEyeY)).dot(Zyx).dot(invS.T)
    A = 0.5*(A.T+A)+np.eye(np.shape(A)[0])*0.000001
    rr, pha = LA.eig(A)
    pha = np.real(pha)
    rr = rr[::-1]
    pha = pha.T[::-1].T
    rr = np.real(rr)
    # sorting out output for alpha
    r = np.diag(np.sqrt(rr))
    alpha = invS.T.dot(pha)
    invRx = Rx.dot(LA.inv(Rx.T.dot(Rx)))
    nalpha = invRx.dot(alpha)
    # sorting out output for beta
    beta = LA.inv(Zyy+kapa*tEyeY).dot(Zyx).dot(alpha)
    t = np.shape(Zyy)[0]
    beta = beta/matlib.repmat(np.diag(r),t,1)
    invRy = Ry.dot(LA.inv(Ry.T.dot(Ry)))
    nbeta = invRy.dot(beta)
    # normalize alpha
    n_a = np.shape(nalpha)[0]
    KKx = Kx.T.dot(Kx)
    compx = np.sqrt(np.diag(nalpha.T.dot(KKx).dot(nalpha)))
    nalpha = nalpha/matlib.repmat(compx,n_a,1)
    # normalize beta
    n_b = np.shape(nbeta)[0]
    KKy = Ky.T.dot(Ky)
    compy = np.sqrt(np.diag(nbeta.T.dot(KKy).dot(nbeta)))
    nbeta = nbeta/matlib.repmat(compy,n_b,1)
    print(np.size(nalpha))
    return nalpha, nbeta, rr

# centering data
def K_mean_center(k):
    l = np.shape(k)[0]
    j = np.ones(l).reshape(-1,1)
    a =  (j.dot(j.T).dot(k))/l 
    b = (k.dot(j).dot(j.T))/l 
    c = (j.T.dot(k).dot(j))[0]
    c = c*j.dot(j.T)/np.square(l)
    k = k-a-b+c
    return k

# decompose kernels
def PGSO(K,T):
    m = np.shape(K)[1]
    index = np.zeros(m)
    tSize = np.zeros(m)
    feat = np.zeros([m,m])
    norm2 = np.diag(K)
    j = 0
    while np.sum(norm2)> T and j != m:
        j2 = np.argmax(norm2)
        index[j] = j2
        tSize[j] = np.sqrt(norm2[j2])
        # calculating new features
        tSum = feat[:,:j].dot(feat[j2,:j].T)
        feat[:,j] = (K[:,j2]-tSum)/tSize[j]
        norm2 = norm2 - np.square(feat[:,j])
        j = j + 1
    return feat, tSize, index

def delZeros(A):
    s1 = A[~np.all(A == 0, axis=1)]
    A = s1.T[~np.all(s1.T == 0, axis=1)]
    return A.T
import collections
# the evaluation approach for the projection matrix
'''
   input: 
          x: float, np.array(n_samples, n_features), the projected random vectors
          y: float, np.array(n_samples, n_features), the projected random vectors
          src_words: string, np.array(n_samples), the source word volcabulary
          trg_words: string, np.array(n_samples), the target word volcabulary
          src_indices_test: int, list(n_samples), the indices of the source words in the dictionary
          trg_indices_test: int, list(n_samples), the indices of the target words in the dictionary
          eval_app: string, the evaluation approach: "nn" ---> nearest neighbours, "CSLS": -->
          batch_size: int, matrix split size
          
    output: float, the translation accuracy for test set
'''
def evalutateTranslation(x, y, src_words, trg_words, src_indices_test,trg_indices_test, eval_app, batch_size):
    # Build word to index map
    x = length_normalize(x)
    y = length_normalize(y)
    src2trg = collections.defaultdict(set)
    
    for src_ind,trg_ind in zip(src_indices_test,trg_indices_test):
        src2trg[src_ind].add(trg_ind)
    
    src = list(src2trg.keys())
    translation = collections.defaultdict(int)
    # nn
    if eval_app == "nn":
        for i in range(0,len(src), batch_size):
            j = min(i + batch_size,len(src))
            similarities = x[src[i:j]].dot(y.T)
            nn = similarities.argmax(axis = 1).tolist()
            for k in range(j-i):
                translation[src[i+k]] = nn[k]
    # csls
    elif eval_app == "CSLS":
  
        rt = np.zeros(y.shape[0], dtype="float32")
        for i in range(0,y.shape[0], batch_size):
            # compute sim with x and y
            j = min(i+batch_size,y.shape[0])
            rt[i:j] = topkMean(y[i:j].dot(x.T), k = 10, inplace=True)


        for i in range(0,len(src),batch_size):
            j = min(i+batch_size,len(src))
            similarities = 2*x[src[i:j]].dot(y.T) - rt  # Equivalent to the real CSLS scores for NN
            nn = similarities.argmax(axis=1).tolist()
            for k in range(j-i):
                translation[src[i+k]] = nn[k]
    
    # Compute accuracy
    accuracy = np.mean(np.array([1 if translation[i] in src2trg[i] else 0 for i in src]))
    
    return accuracy


def topkMean(m, k=10, inplace=False):
    n = m.shape[0]
    ans = np.zeros(n)
    if k <= 0:
        return ans
    if not inplace:
        m = np.array(m)
    ind0 = np.arange(n)
    ind1 = np.empty(n, dtype=int)
    minimum = m.min()
    for i in range(k):
        #find the maximun one
        m.argmax(axis=1, out=ind1)
        #add to ans
        ans += m[ind0, ind1]
        # replace them into the minimun one
        m[ind0, ind1] = minimum
    return ans / k

def whitening_transformation(m):
    u, s, vt = np.linalg.svd(m, full_matrices=False)
    return vt.T.dot(np.diag(1/s)).dot(vt)


def length_normalize_dimension_wise(x):
    norms = np.sqrt(np.sum(x**2, axis=0))
    return x/norms

def energy(r,limit):
    a = list(reversed(r))
    dimi = 0
    sumi = 0
    for i in range(len(a)):
        sumi += a[i]
        if sumi/np.sum(a) >= limit:
            dimi = i
            break

    return dimi

def run(vol_src,emb_src,vol_trg,emb_trg,src_indices_train,trg_indices_train,src_indices_test, trg_indices_test, **args):

    #step 1: data preprocessing: length_normalize& mean centering
    emb_src = length_normalize(emb_src)
    emb_trg = length_normalize(emb_trg)
    emb_src = mean_center(emb_src)
    emb_trg = mean_center(emb_trg)
    x = emb_src[src_indices_train]
    y = emb_trg[trg_indices_train]    


    # step 2: Kernelization
    Kx = rbf_kernel(x, gamma=args["gamma"])
    Ky = rbf_kernel(y, gamma=args["gamma"])

    # Step3: KCCA
    wx, wy, r = hardoonKCCA(Kx,Ky, args["kappa"], args["yita"])
    
    wx = np.array(wx, dtype = "float32") 
    wy = np.array(wy, dtype = "float32") 
    
    # Step4 : kerelize new data
    x_new = rbf_kernel(emb_src,x, gamma=args["gamma"])
    y_new = rbf_kernel(emb_trg,y, gamma=args["gamma"])

    
    # step5: projection
    projs = x_new.dot(wx)
    projt = y_new.dot(wy)
    
    # reweighting
    projs *= r**args["weight"]
    projt *= r**args["weight"]

    # step6 : evaluation
    acc = evalutateTranslation(projs, projt, vol_src, vol_trg, src_indices_test, trg_indices_test,"CSLS",5000)
    print("evaluation end, the acuarccy is: ",acc,"the dimension is: ", np.shape(projs)[1])


    return acc



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="global argument group")
    #path
    parser.add_argument("--src_path", default = 'embeddings/en.emb.txt', help = "input the path of source word embeddings")
    parser.add_argument("--trg_path", default = 'embeddings/zh.emb.txt', help = "input the path of target word embeddings")
    parser.add_argument("--dic_train_path", default = 'dictionary/en-zh.train.txt', help = "input the path of training dicitonary")
    parser.add_argument("--dic_test_path", default = 'dictionary/en-zh.test.txt', help = "input the path of training dicitonary")
    # parameter for kernels

    parser.add_argument("--gamma", default=0.21, type=float, help='hyperparameter for rbf kernel')
    # parameter for KCCA
    parser.add_argument("--kappa", default=10, type=float, help ="regularzation paremeter")
    parser.add_argument("--yita", default=0.7, type=float, help ="PGSO parameter")   
    # parameter for projection
    parser.add_argument("--weight", default=0.50, type=float, help="reweighting")
    # print("reading data...")
    args = parser.parse_args()
    data_path = [args.src_path,args.trg_path,args.dic_train_path, args.dic_test_path]
    
    vol_src,emb_src,vol_trg,emb_trg,src_indices_train,trg_indices_train,src_indices_test, trg_indices_test = load_data(data_path)
    paraSets = {
    'gamma' : args.gamma,
    'kappa' : args.kappa,
    'yita' : args.yita,
    'weight' : args.weight
    }
    print(paraSets)
    acc = run(vol_src,emb_src,vol_trg,emb_trg,src_indices_train,trg_indices_train,src_indices_test, trg_indices_test, **paraSets)
